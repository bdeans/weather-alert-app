import React from 'react';
import ReactDOM from 'react-dom';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from './App';
import SearchCity from '../components/SearchCity';
import Result from '../components/Result';

configure({ adapter: new Adapter() });

describe('App', () => {
  let props;
  let mountedApp;
  const app = () => {
    if (!mountedApp) {
      mountedApp = mount(<App />);
    }
    return mountedApp;
  };

  beforeEach(() => {
    mountedApp = undefined;
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('always renders a `SearchCity`', () => {
    expect(app().find(SearchCity).length).toBe(1);
  });

  it("doesn't render a `Result at first`", () => {
    expect(app().find(Result).length).toBe(0);
  });

  describe('`SearchCity`', () => {
    const change = jest.fn();
    const submit = jest.fn();
    const showResult = true;
    const searchCity = shallow(
      <SearchCity value={'testLocation'} showResult={showResult} change={change} submit={submit} />,
    );

    it('receives its props', () => {
      expect(Object.keys(searchCity.props()).length).toBe(1);
    });
  });
});
