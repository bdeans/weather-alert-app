# Weather Alert app

Weather Alert application

## Table of contents
* [Instructions](#Instructions)
* [Description](#Description)
* [Technologies](#Technologies)


## Instructions

First clone this repository.
```bash
$ git clone https://gitlab.com/bdeans/weather-alert-app.git
```

Install dependencies. Make sure you already have [`nodejs`](https://nodejs.org/en/) & [`npm`](https://www.npmjs.com/) installed in your system.
```bash
$ npm install # or yarn
```

Run it
```bash
$ npm start # or yarn start
```

## Description
This is a simple React weather app using OpenWeatherMap API.
 
## Technologies
Project uses:
* React
* Styled Components

